/*
 * ConfigButtons1.h
 *
 * Created: 30/06/2016 4:38:30 p.m.
 *  Author: Ned van Geffen - King of Kings
 */ 


#ifndef CONFIGBUTTONS1_H_
#define CONFIGBUTTONS1_H_

// Defines the number of buttons per module. 8 means lots of things are bytes. 16, uint16's and 32 uint32's. Smaller the better.
#define MAX_BUTTONS_PER_MODULE	8

#if MAX_BUTTONS_PER_MODULE > 32
#error "Buttons: No more than 32b supported. Modify at your own risk"
#endif

#endif /* CONFIGBUTTONS1_H_ */
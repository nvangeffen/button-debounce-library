Welcome to my (way over the top) debounce code!

Many years ago i wrote the main part of this code that detects button presses, holds and releases. Its worked quite well for me, and even made it onto hackadays website (i sent it in myself, but still, they listed it ;) )

This version has some improvements though! It still does button presses, holds and releases, but now also supports callbacks, as well as multiple button groups and run time adjustment of what buttons are being sampled.

There is a Atmel Studio 6.2 project in the repo with a silly little example that shows how to setup the whole button module etc. but as a quick explanation of how it works;

First you make a new module buttonsModule.
To setup the module, create a buttonsConfig struct. The config struct can be filled in with defaults using the ButtonsGetConfigDefaults function.
Now initialise the buttons module using ButtonsInit, and pass it the module, your time reference (ms tick timer is great) and your config struct.

Now we need to add some buttons. By default it works with up to 8 buttons per module. This is a byte, which makes life easy. Theres a setting in the config file to change it though.
each button is represented by a bit, and when adding a buttion, you specify which bit you want that button to represent.
So ButtonsSetButton adds a button to the module, and you set the port it is on, the bit it is in the port and if its active low or not. The last argument is for which bit the button represents.
Buttons arent enabled by default, so after adding one, you should enable it using ButtonsEnableButton

Now you can set the optional callbacks as well, for pressed, held and/or released, using ButtonsRegisterCallback and the module, function pointer and the type of callback.
Again, the callback will have to be enabled using ButtonsEnableCallback.

All thats left is to add the module to the task handler using ButtonsAddModule, and its all set up!

In the main loop, just call the ButtonTaskHandler every so often, and it will keep it all up to scratch for you.
There is no need to call the handler as quickly as possible. I normally call it every 5mS and thats more than fast enough!

The example code has 2 buttons in 1 module, and takes ~1000 clock cycles with no buttons pressed to service the task handler (aka ~50-60uS on a 16 or 20MHz clock)
So if you call it every 5ms, it uses ~1.2% of the CPU time for some very advanced button handing code with callbacks and stuff. Not bad in my book.
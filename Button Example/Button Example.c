/*
 * Button_Example.c
 *
 * Created: 1/07/2016 3:58:53 p.m.
 *  Author: Ned
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "..\Buttons.h"

#define SWITCH_1			(1 << 7)
#define SWITCH_2			(1 << 6)

#define GRN_LED			(PINA  &   (1 << 0))
#define GRN_LED_ON		(PORTA |=  (1 << 0))
#define GRN_LED_OFF		(PORTA &= ~(1 << 0))
#define GRN_LED_TGL		(PORTA ^=  (1 << 0))

#define RED_LED			(PIND  &   (1 << 5))
#define RED_LED_ON		(PORTD |=  (1 << 5))
#define RED_LED_OFF		(PORTD &= ~(1 << 5))
#define RED_LED_TGL		(PORTD ^=  (1 << 5))

struct buttonsModule userButtons;

volatile uint16_t isrTime;

void Pressed_Handler(struct buttonsModule *const module, uint32_t buttons) {
	if (buttons == SWITCH_1) {
		// Just switch 1 pressed
		RED_LED_TGL;
	}
	
	if (buttons == SWITCH_2) {
		// Just switch 2 pressed
		GRN_LED_TGL;
	}
}

void Held_Handler(struct buttonsModule *const module, uint32_t buttons) {
	if (buttons == SWITCH_1) {
		// Just switch 1 pressed
		RED_LED_TGL;
		GRN_LED_OFF;
	}
	
	if (buttons == SWITCH_2) {
		// Just switch 2 pressed
		RED_LED_OFF;
		GRN_LED_TGL;
	}
	
	if (buttons == (SWITCH_1 | SWITCH_2)) {
		// Both switch pressed
		RED_LED_TGL;
		GRN_LED_TGL;
	}
}

void Release_Handler(struct buttonsModule *const module, uint32_t buttons) {
	if (buttons == (SWITCH_1 | SWITCH_2)) {
		// both switches released
		RED_LED_ON;
		GRN_LED_ON;
	}
	else if ((buttons & SWITCH_1) || (buttons & SWITCH_2)) {
		// either switch released
		RED_LED_OFF;
		GRN_LED_OFF;
	}
}

// Set the required I/O
void SetupPorts(void) {
	PORTC = 0xC0;	// Pullups Buttons
	
	DDRA = 0x01;		// Green LED
	DDRD = 0x20;		// Red LED
	
	GRN_LED_ON;
}

// Create a mS timer
void SetupTimers(void) {
	TCCR0A = 0x02;
	TCCR0B = 0x03;
	OCR0A = 249;
	TIMSK0 = 0x02;
}

// Init the buttons
void SetupButtons(void) {
	// Create a config struct
	struct buttonsConfig config;
	
	ButtonsGetConfigDefaults(&config);														// Set config struct to defaults
	ButtonsInit(&userButtons,  (uint16_t *)&isrTime, &config);								// Init the button module with the timer it points to, and the config you've set up
	
	ButtonsSetButton(&userButtons, &PINC, 7, 1, 7);											// Set user button index 7, to PINC.7, active low
	ButtonsSetButton(&userButtons, &PINC, 6, 1, 6);											// Set user button index 6, to PINC.6, active low
	ButtonsEnableButton(&userButtons, 7);													// Enable button index 7
	ButtonsEnableButton(&userButtons, 6);													// Enable button index 6	
	
	ButtonsRegisterCallback(&userButtons, Pressed_Handler, BUTTONS_CALLBACK_PRESSED);		// Register a callback for the pressed callback
	ButtonsRegisterCallback(&userButtons, Held_Handler, BUTTONS_CALLBACK_HELD);				// Register a callback for the held callback
	ButtonsRegisterCallback(&userButtons, Release_Handler, BUTTONS_CALLBACK_RELEASED);		// Register a callback for the released callback
	
	ButtonsEnableCallback(&userButtons, BUTTONS_CALLBACK_PRESSED);							// Enable the callbacks
	ButtonsEnableCallback(&userButtons, BUTTONS_CALLBACK_HELD);
	ButtonsEnableCallback(&userButtons, BUTTONS_CALLBACK_RELEASED);
	
	ButtonsAddModule(&userButtons);															// Add the module to the task handler
}

int main(void)
{
	SetupPorts();
	SetupButtons();
	SetupTimers();
	
	sei();
		
	while (1) {
		ButtonTaskHandler();																	// Task handler that checks all the modules
		
		// Run whatever other code you want to run here....
	}
}

// just increment a counter every mS
ISR(TIMER0_COMPA_vect) {
	isrTime++;
}
